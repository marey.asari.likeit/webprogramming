<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ削除画面</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" href="css/header.css">
<link rel="stylesheet" href="css/login.css">
</head>

<body>
<!-- ヘッダー -->
<nav class="navbar navbar-light" style="background-color: #e6e6fa;">
	<ul class="navbar-nav navbar-light flex-row mr-auto">
        <li class="nav-item active">
          <a class="navbar-brand" href="userRegister.html">ユーザ管理システム</a>
        </li>
      </ul>
        <ul class="nav navbar-nav navbar-right">
            <li class="navbar-text">${userInfo.name} さん </li>
  			<li class="dropdown">
        <li class="nav-item">
          <a href="LogoutServlet" class="navbar-link logout-link">ログアウト</a>
        </li>
      </ul>
</nav>

<!-- フォーム -->
<div class="container">
    <div class="area">
    <form class="form-register" action="UserDeleteServlet" method="post">
    <input type="hidden" name="id" value="${user.id}">
    
      <p>${user.loginId}を消去しますか？</p>
      <div class="row">
        <div class="col-sm-6">
          <a href="UserListServlet" class="btn btn-outline-secondary btn-block">いいえ</a>
        </div>
        <div class="col-sm-6">
          <button type="submit" class="btn btn-outline-primary btn-block">はい</button>
        </div>
      </div>
      </form>
    </div>
  </div>
</body>

</html>