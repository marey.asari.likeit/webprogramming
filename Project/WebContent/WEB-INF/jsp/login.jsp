<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ログイン画面</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="css/login.css">
</head>
<body>

<nav class="navbar navbar-light" style="background-color: #e6e6fa;">
	<ul class="navbar-nav navbar-light flex-row mr-auto">
        <li class="nav-item active">
          <a class="navbar-brand" href="UserRegisterServlet">ユーザ管理システム</a>
        </li>
      </ul>
</nav>

	<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>
	
	<form class="form-signin" action="LoginServlet" method="post">
		<div class="area">
		<div class="text-center">
			<h4 class="title">ログイン画面</h4>
				<div class="middle">
			<input type="text" name="loginId" id="inputLoginId" class="form-control" placeholder="ログインID" required autofocus>
				</div>
			<div class="end">
				<input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
			</div>
			<button class="btn btn-primary" type="submit">ログイン</button>
		</div>
		</div>
	</form>
</body>
</html>