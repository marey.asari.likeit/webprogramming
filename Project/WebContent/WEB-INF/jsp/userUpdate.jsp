<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ更新画面</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" href="css/header.css">
<link rel="stylesheet" href="css/user.css">
</head>

<body>
<!-- ヘッダー -->
<nav class="navbar navbar-light" style="background-color: #e6e6fa;">
	<ul class="navbar-nav navbar-light flex-row mr-auto">
        <li class="nav-item active">
          <a class="navbar-brand" href="userRegister.html">ユーザ管理システム</a>
        </li>
      </ul>
        <ul class="nav navbar-nav navbar-right">
            <li class="navbar-text">${userInfo.name} さん </li>
  			<li class="dropdown">
        <li class="nav-item">
          <a href="LogoutServlet" class="navbar-link logout-link">ログアウト</a>
        </li>
      </ul>
</nav>

<!-- フォーム -->
<div class="form">
<form class="form-update" action="UserUpdateServlet" method="post">
	<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>
<div class="data">


<input type="hidden" name="id" value="${user.id}">

<div class="form-groups row">
        <label for="loginId" class="col-sm-2 col-form-label">ログインID</label>
        <div class="col-sm-10">
          <p class="form-control-plaintext">${user.loginId}</p>
        </div>
      </div>

     <div class="form-groups row">
        <label for="password" class="col-sm-2 col-form-label">パスワード</label>
        <div class="col-sm-10">
          <input type="password" name="password" id="inputPassword" class="form-control">
        </div>
      </div>

      <div class="form-groups row">
        <label for="confirmPassword" class="col-sm-2 col-form-label">パスワード(確認)</label>
        <div class="col-sm-10">
          <input type="password" name="confirm" id="inputPassword" class="form-control">
        </div>
      </div>

      <div class="form-groups row">
        <label for="userName" class="col-sm-2 col-form-label">ユーザ名</label>
        <div class="col-sm-10">
          <input type="text" name="name" id="user-name" class="form-control" value="${user.name}">
        </div>
      </div>

      <div class="form-groups row">
        <label for="birthday" class="col-sm-2 col-form-label">生年月日</label>
        <div class="col-sm-10">
          <input type="date" name="birthDate" id="birthday" class="form-control" value="${user.birthDate}">
        </div>
      </div>
</div>
	<div class="button">
	<button type="submit" class="btn btn-outline-primary btn-lg btn-block">登録</button>
	</div>
	<a href="UserListServlet">戻る</a>
</form>
</div>
</body>

</html>