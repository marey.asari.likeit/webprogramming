<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ一覧画面</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="css/header.css">
	<link rel="stylesheet" href="css/user.css">
</head>

<body>
<!-- ヘッダー -->
<nav class="navbar navbar-light" style="background-color: #e6e6fa;">
	<ul class="navbar-nav navbar-light flex-row mr-auto">
        <li class="nav-item active">
          <a class="navbar-brand" href="UserRegisterServlet">ユーザ管理システム</a>
        </li>
      </ul>
        <ul class="nav navbar-nav navbar-right">
            <li class="navbar-text">${userInfo.name} さん </li>
  			<li class="dropdown">
        <li class="nav-item">
          <a href="LogoutServlet" class="navbar-link logout-link">ログアウト</a>
        </li>
      </ul>
</nav>

<div class="addUser">
<a class="btn btn-outline-primary btn-lg" href="UserRegisterServlet">新規登録</a>
</div>

<!-- 入力フォーム -->
<div class="area">
<form class="form-search" action="UserListServlet" method="post">
  <div class="form-crowd row">
    <label for="code" class="control-label col-sm-2">ログインID</label>
    <div class="col-sm-10">
      <input type="text" name="loginId" id="login-id" class="form-control"/>
    </div>
  </div>
  <div class="form-crowd row">
    <label for="name" class="control-label col-sm-2">ユーザ名</label>
    <div class="col-sm-10">
      <input type="text" name="name" id="user-name" class="form-control"/>
    </div>
  </div>
  <div class="form-crowd row">
    <label for="continent" class="control-label col-sm-2">生年月日</label>
     <div class="row col-sm-10">
             <div class="col-sm-5">
               <input type="date" name="dateStart" id="date-start" class="form-control" >
             </div>

             <div class="col-sm-1 text-center">
               ~
             </div>
             <div class="col-sm-5">
               <input type="date" name="dateEnd" id="date-end" class="form-control" />
             </div>
           </div>
  </div>
  <div class="search">
  <button type="submit" class="btn btn-primary">検索</button>
  </div>
  </form>
  </div>
  
<!-- ユーザ一覧 -->
<div class="table-responsive">
<table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">ログインID</th>
      <th scope="col">ユーザ名</th>
      <th scope="col">生年月日</th>
      <th scope="col"></th>
    </tr>
  </thead>
			<tbody>
                 <c:forEach var="user" items="${userList}" >
                   <tr>
                     <td>${user.loginId}</td>
                     <td>${user.name}</td>
                     <td>${user.birthDate}</td>
                     
                     <!-- TODO 未実装；ログインボタンの表示制御を行う -->
                     <c:if test="${userInfo.loginId == 'admin'}" >
                     <td>
                       <a class="btn btn-outline-primary" href="UserDetailServlet?id=${user.id}">詳細</a>
                       <a class="btn btn-outline-success" href="UserUpdateServlet?id=${user.id}">更新</a>
                       <a class="btn btn-outline-danger" href ="UserDeleteServlet?id=${user.id}">削除</a>
                     </td>
                     </c:if>
                     
                     <c:if test="${userInfo.loginId != 'admin'}" >
                     <td>
                       <a class="btn btn-outline-primary" href="UserDetailServlet?id=${user.id}">詳細</a>
                       <c:if test="${userInfo.loginId == user.loginId}" >
                       <a class="btn btn-outline-success" href="UserUpdateServlet?id=${user.id}">更新</a>
                       </c:if>
                     </td>
                     </c:if>
                     
                   </tr>
                 </c:forEach>
			</tbody>
</table>
</div>
</body>

</html>