<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ登録画面</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" href="css/header.css">
<link rel="stylesheet" href="css/user.css">
</head>

<body>
<!-- ヘッダー -->
<nav class="navbar navbar-light" style="background-color: #e6e6fa;">
	<ul class="navbar-nav navbar-light flex-row mr-auto">
        <li class="nav-item active">
          <a class="navbar-brand" href="UserRegisterServlet">ユーザ管理システム</a>
        </li>
      </ul>
        <ul class="nav navbar-nav navbar-right">
            <li class="navbar-text">${userInfo.name} さん </li>
  			<li class="dropdown">
        <li class="nav-item">
          <a href="LogoutServlet" class="navbar-link logout-link">ログアウト</a>
        </li>
      </ul>
</nav>

	<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>

<!-- 入力フォーム -->
<form class="form-register" action="UserRegisterServlet" method="post">
  <div class="form-group">
    <label for="loginId">ログインID</label>
    <input type="text" name="loginId" class="form-control" id="registerLoginId">
  </div>
  <div class="form-group">
    <label for="password">パスワード</label>
    <input type="password" name="password" class="form-control" id="registerPassword">
  </div>
  <div class="form-group">
    <label for="confirmPassword">パスワード(確認)</label>
    <input type="password" name="confirm" class="form-control" id="confirmPassword">
  </div>
  <div class="form-group">
    <label for="inputUserName">ユーザ名</label>
    <input type="text" name="name" class="form-control" id="registerUserName">
  </div>
  <div class="form-group">
    <label for="birthday">生年月日</label>
    <div class="row col-sm-10">
             <div class="col-sm-5">
               <input type="date" name="birthDate" id="registerBirthDate" class="form-control" />
             </div>
    </div>
   <div class="register">
   <button type="submit" class="btn btn-outline-primary">登録</button>
   </div>
   <div class="return">
   <a href="UserListServlet">戻る</a>
   </div>
   </div>
</form>
</body>

</html>