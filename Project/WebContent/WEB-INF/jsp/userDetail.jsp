<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ詳細画面</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" href="css/header.css">
<link rel="stylesheet" href="css/user.css">
</head>
<body>
<!-- ヘッダー -->
<nav class="navbar navbar-light" style="background-color: #e6e6fa;">
	<ul class="navbar-nav navbar-light flex-row mr-auto">
        <li class="nav-item active">
          <a class="navbar-brand" href="userRegister.html">ユーザ管理システム</a>
        </li>
      </ul>
        <ul class="nav navbar-nav navbar-right">
            <li class="navbar-text">${userInfo.name} さん </li>
  			<li class="dropdown">
        <li class="nav-item">
          <a href="LogoutServlet" class="navbar-link logout-link">ログアウト</a>
        </li>
      </ul>
</nav>

<!-- フォーム -->
<div class="form">
	
<div class="data">
  <div class="form-crowd row">
    <label for="code" class="control-label col-sm-2">ログインID</label>
    <div class="col-sm-10">
        <p class="form-control-plaintext">${user.loginId}</p>
    </div>
  </div>

  <div class="form-crowd row">
    <label for="name" class="control-label col-sm-2">ユーザ名</label>
    <div class="col-sm-10">
        <p class="form-control-plaintext">${user.name}</p>
    </div>
  </div>

  <div class="form-crowd row">
    <label for="continent" class="control-label col-sm-2">生年月日</label>
     <div class="row col-sm-10">
         <p class="form-control-plaintext">${user.birthDate}</p>
     </div>
  </div>

      <div class="form-crowd row">
        <label for="createDate" class="col-sm-2 col-form-label">新規登録日時</label>
        <div class="col-sm-10">
            <p class="form-control-plaintext">${user.createDate}</p>
        </div>
      </div>

      <div class="form-crowd row">
        <label for="updateDate" class="col-sm-2 col-form-label">更新日時</label>
        <div class="col-sm-10">
          <p class="form-control-plaintext">${user.updateDate}</p>
        </div>
      </div>
</div>


	<a href="UserListServlet">戻る</a>
</div>
</body>

</html>